# Geopins: Building a Realtime App with React Hooks and GraphQL

This project contains code from studying [Building a Realtime App with React Hooks and GraphQL](https://www.udemy.com/course/build-a-realtime-app-with-react-hooks-and-graphql) course.

## 🏠 [Homepage](https://bitbucket.org/ahristov/geopins/src)

## Install

```sh
yarn
cd client
yarn
```

## Run

To run the server in dev mode:

```sh
yarn run dev
```

To run the client app:

```sh
cd client
yarn start
```

## Dev support

Install ESLint:

```sh
yarn add -D eslint eslint-plugin-react typescript-eslint @typescript-eslint/eslint-plugin @typescript-eslint/parser
./node_modules/.bin/eslint --init
```

## Notes

### Create react app with TypeScript

```sh
npx create-react-app client --typescript
```

### Dependencies

Installed dependencies on the server:

```sh
yarn add dotenv mongoose graphql google-auth-library apollo-server nodemon
```

Installed dependencies on the client:

```sh
cd client
yarn add @types/react-router-dom @material-ui/core @material-ui/icons apollo-cache-inmemory @types/react-map-gl apollo-client apollo-link-ws axios date-fns graphql graphql-request graphql-tag mapbox-gl react-apollo react-google-login react-map-gl react-router-dom subscriptions-transport-ws
```

For _hot reloading type definitions_, install:

```sh
cd client
yarn add @types/webpack-env
```

You can find `module.hot` usage in `client/index.tsx`.

To install _Google API type definitions_, run:

```sh
cd client
yarn add @types/gapi @types/gapi.auth2
```

Observe modules are installed under:

```sh
node_modules
  @types\
    gapi
    gapi.auth2
```

Note: if you modify `tsconfig.json` from `client` to include gapi and gapi.auth2 types:

```json
{
  "compilerOptions": {
    "types": ["gapi", "gapi.auth2"],
...
```

, then all the types have to be added (module.hot stops working). Looks like somehow these are automatically added.

See: [Import gapi.auth2 in angular 2 typescript](https://stackoverflow.com/questions/38091215/import-gapi-auth2-in-angular-2-typescript/50616780)

## Author

👤 **Atanas Hristov**

## Course notes

### Links

[Building a Realtime App with React Hooks and GraphQL](https://www.udemy.com/course/build-a-realtime-app-with-react-hooks-and-graphql)  
[Geopins original repo](https://github.com/reedbarger/geopins)  
[Create-react-app with TypeScript](https://www.pluralsight.com/guides/typescript-react-getting-started)  
[Google Developers Console, used for OAuth](console.developers.google.com)  
[Mongo DB Atlas](cloud.mongodb.com)  
[Mapbox](https://www.mapbox.com/)  
[Cloudinary image processing](https://cloudinary.com/)  
