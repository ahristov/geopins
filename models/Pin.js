const mongoose = require('mongoose')

// _id is automatically created
const PinSchema = new mongoose.Schema(
  {
    title: String,
    content: String,
    image: String,
    latitude: Number,
    longitude: Number,
    author: { type: mongoose.Schema.ObjectId, ref: "User" },
    comments: [
      {
        text: String,
        createdAt: { type: Date, default: Date.now },
        author: { type: mongoose.Schema.ObjectId, ref: "User" },
      }
    ]
  },
  // whenever new Pin is created, it will set createdAt with it
  { timestamps: true }
)

module.exports = mongoose.model("Pin", PinSchema)