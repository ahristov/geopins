const mongoose = require('mongoose')

// _id is automatically created
const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  picture: String
})

module.exports = mongoose.model("User", UserSchema)