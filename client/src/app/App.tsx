import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomePage from '../pages/HomePage'
import SplashPage from '../pages/SplashPage'
import AppContext from './AppContext'
import ProtectedRoute from './ProtectedRoute'

const App: React.FC = () => {
  return (
      <Router>
        <AppContext.Provider>
          <Switch>
            <ProtectedRoute exact={true} path="/" component={HomePage} />
            <Route path="/login" component={SplashPage} />
          </Switch>
        </AppContext.Provider>
      </Router>
  );
}

export default App;
