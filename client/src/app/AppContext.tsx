import React from 'react'

interface IUser {
  name: string,
  email: string,
  picture: string
}

interface IAction {
  type: string
  payload?: any
}

type DispatchType = (action: IAction) => void

export const ACTIONS = {
  LOGIN_USER: "LOGIN_USER",
  IS_LOGGED_IN: "IS_LOGGED_IN",
  SIGNOUT_USER: "SIGNOUT_USER",
  CREATE_DRAFT: "CREATE_DRAFT",
  UPDATE_DRAFT_LOCATION: "UPDATE_DRAFT_LOCATION",
  DELETE_DRAFT: "DELETE_DRAFT",
  GET_PINS: "GET_PINS",
  CREATE_PIN: "CREATE_PIN"
}

// Context

interface IState {
  dispatch: DispatchType,
  currentUser?: IUser,
  isAuth: boolean,
  draft?: {latitude: number, longitude: number}
  pins?: any[]
}

// Context

const Context = React.createContext<IState>({} as IState)


// Consumer

const Consumer = Context.Consumer


// Reducer

const reducer = (state: IState, { type, payload }: IAction) => {
  switch (type) {
    case ACTIONS.LOGIN_USER:
      return {
        ...state,
        currentUser: payload
      }
    case ACTIONS.IS_LOGGED_IN:
      return {
        ...state,
        isAuth: payload
      }
    case ACTIONS.SIGNOUT_USER:
      return {
        ...state,
        isAuth: false,
        currentUser: null
      }
    case ACTIONS.CREATE_DRAFT:
      return {
        ...state,
        draft: {
          latitude: 0,
          longitude: 0
        }
      }
    case ACTIONS.UPDATE_DRAFT_LOCATION:
      return {
        ...state,
        draft: payload
      }
    case ACTIONS.DELETE_DRAFT:
      return {
        ...state,
        draft: null
      }
    case ACTIONS.GET_PINS:
      return {
        ...state,
        pins: payload
      }
    case ACTIONS.CREATE_PIN:
      const newPin = payload
      const prevPins = state.pins ? state.pins.filter(pin => pin._id !== newPin._id) : []
      return {
        ...state,
        pins: [...prevPins, newPin]
      }
    default:
      return state
  }
}

// Provider

class Provider extends React.Component<{}, IState> {
  constructor(props: {}) {
    super(props)

    this.state = {
      dispatch: (action: IAction) => {
        this.setState(state => reducer(state as IState, action))
      },
      currentUser: undefined,
      isAuth: false,
      draft: undefined,
    }
  }

  render = () => <Context.Provider value={this.state}>{this.props.children}</Context.Provider>
}


export default { Context, Consumer, Provider }
