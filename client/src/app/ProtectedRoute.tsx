import React, { ReactType } from 'react';
import { Route, Redirect } from 'react-router-dom'
import AppContext from './AppContext'

interface IProtectedRouteProps {
  component: ReactType,
  path: string,
  exact: boolean | undefined
}

const ProtectedRoute: React.FC<IProtectedRouteProps> = ({
  component:Component, path, exact,
  ...rest}) => {
  return (
    <AppContext.Consumer>
      {({ isAuth }) =>
        <Route exact={exact} path={path} render={props =>
          !isAuth ? <Redirect to="/login" /> : <Component {...props} />}
          {...rest} />
      }
    </AppContext.Consumer>
  )
}

export default ProtectedRoute;
