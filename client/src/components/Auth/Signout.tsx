import React, { useContext } from 'react'
import { GoogleLogout } from 'react-google-login'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
import ExitToAppIcon from "@material-ui/icons/ExitToApp"
import Typography from "@material-ui/core/Typography"
import AppContext, { ACTIONS } from '../../app/AppContext'

const Signout: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  const { dispatch } = useContext(AppContext.Context)

  const onSignout = () => {
    dispatch({ type: ACTIONS.SIGNOUT_USER })
  }

  return (
        <GoogleLogout
          clientId="13353757784-hdqksdg750vkbg5h7s91jias7ahpd62f.apps.googleusercontent.com"
          onLogoutSuccess={onSignout}
          render={({ onClick }) => (
            <span className={classes.root} onClick={onClick}>
              <Typography
                variant="body1"
                className={classes.buttonText}
              >
                Signout
              </Typography>
              <ExitToAppIcon className={classes.buttonIcon} />
            </span>
          )}
        />
  )
}

const styles = (theme: Theme) => createStyles({
  root: {
    cursor: "pointer",
    display: "flex"
  },
  buttonText: {
    color: "orange"
  },
  buttonIcon: {
    marginLeft: "5px",
    color: "orange"
  }
});

export default withStyles(styles)(Signout);

