import React, { useContext } from 'react'
import { GraphQLClient } from 'graphql-request'
import { GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"

import AppContext, { ACTIONS } from '../../app/AppContext'
import { ME_QUERY } from '../../graphql/queries'
import { BASE_URL } from '../../shared/graphql'

const Login: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  const { dispatch } = useContext(AppContext.Context)

  const gotResponse = (googleUser: GoogleLoginResponse | GoogleLoginResponseOffline): googleUser is GoogleLoginResponse => true

  const onSuccess = async (googleUser: GoogleLoginResponse | GoogleLoginResponseOffline) => {
    try {
      if (gotResponse(googleUser))
      {
         const idToken = googleUser.getAuthResponse().id_token
         const client = new GraphQLClient(BASE_URL, {
           headers: { authorization: idToken }
         })
         const { me } = await client.request(ME_QUERY)
         dispatch({ type: ACTIONS.LOGIN_USER, payload: me })
         dispatch({ type: ACTIONS.IS_LOGGED_IN, payload: googleUser.isSignedIn() })
      }
    } catch (err) {

    }
  }

  const onFailure = (err: any) => {
    console.error('Google auth error', err)
  }

  return (
    <div className={classes.root}>
      <Typography
        component="h1"
        variant="h3"
        gutterBottom
        noWrap
        className={classes.welcome}
      >
        Welcome
      </Typography>
      <GoogleLogin
        clientId="13353757784-hdqksdg750vkbg5h7s91jias7ahpd62f.apps.googleusercontent.com"
        onSuccess={onSuccess}
        onFailure={onFailure}
        isSignedIn={true}
        buttonText="Login with Google"
        theme="dark"
      />
    </div>
  )
}

const styles = (theme: Theme) => createStyles({
  root: {
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center"
  },
  welcome: {
    color: "rgb(66, 133, 244)"
  }
});

export default withStyles(styles)(Login);
