import React, { useContext } from 'react'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
import { Paper } from "@material-ui/core"
import AppContext from '../app/AppContext'
import NoContent from './Pin/NoContent'
import CreatePin from './Pin/CreatePin'

const Blog: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  const { draft } = useContext(AppContext.Context)

  let BlogContent:any;

  if (!draft) {
    BlogContent = NoContent
  } else if (draft) {
    BlogContent = CreatePin
  }

  return (
    <Paper className={classes.root}>
      <BlogContent/>
    </Paper>
  )
}

const styles = (theme: Theme) => createStyles({
  root: {
    minWidth: 350,
    maxWidth: 400,
    maxHeight: "calc(100vh - 64px)",
    overflowY: "scroll",
    display: "flex",
    justifyContent: "center"
  },
  rootMobile: {
    maxWidth: "100%",
    maxHeight: 300,
    overflowX: "hidden",
    overflowY: "scroll"
  }
});

export default withStyles(styles)(Blog);

