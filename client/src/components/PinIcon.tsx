import React from 'react'
import PlaceTwoTone from "@material-ui/icons/PlaceTwoTone";

interface IPinIconProps {
  fontSize?: string | number
  color?: string,
  onClick: (event: React.MouseEvent) => void
}
const PinIcon: React.FC<IPinIconProps> = ({fontSize, color, onClick}) => {
  return (
    <PlaceTwoTone
      style={{ fontSize: fontSize, color: color}}
      onClick={onClick}
    />
  )
}

export default PinIcon
