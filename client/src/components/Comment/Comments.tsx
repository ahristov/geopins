import React from 'react'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
// import List from "@material-ui/core/List"
// import ListItem from "@material-ui/core/ListItem"
// import ListItemText from "@material-ui/core/ListItemText"
// import ListItemAvatar from "@material-ui/core/ListItemAvatar"
// import Avatar from "@material-ui/core/Avatar"
// import Typography from "@material-ui/core/Typography"

const Comments: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  return <div>Comments</div>
}

const styles = (theme: Theme) => createStyles({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: "inline"
  }
});

export default withStyles(styles)(Comments);
