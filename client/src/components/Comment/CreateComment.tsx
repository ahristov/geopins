import React from 'react'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
// import InputBase from "@material-ui/core/InputBase"
// import IconButton from "@material-ui/core/IconButton"
// import ClearIcon from "@material-ui/icons/Clear"
// import SendIcon from "@material-ui/icons/Send"
// import Divider from "@material-ui/core/Divider"

const CreateComment: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  return <div>CreateComment</div>
}

const styles = (theme: Theme) => createStyles({
  form: {
    display: "flex",
    alignItems: "center"
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  clearButton: {
    padding: 0,
    color: "red"
  },
  sendButton: {
    padding: 0,
    color: theme.palette.secondary.dark
  }
});

export default withStyles(styles)(CreateComment);
