import React from 'react'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import MapIcon from "@material-ui/icons/Map"
import Typography from "@material-ui/core/Typography"
import AppContext from '../app/AppContext'
import Signout from '../components/Auth/Signout'

const Header: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  return (
    <AppContext.Consumer>
      {({ currentUser }) =>
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              {/* Title / Logo */}
              <div className={classes.grow}>
                <MapIcon className={classes.icon} />
                <Typography
                  component="h1"
                  variant="h6"
                  color="inherit"
                  noWrap
                >
                  GeoPins
                </Typography>
              </div>

              {/* Current User Info */}
              {currentUser && (
                <div className={classes.grow}>
                  <img
                    className={classes.picture}
                    src={currentUser.picture}
                    alt={currentUser.name}
                  />
                  <Typography
                    variant="h5"
                    color="inherit"
                    noWrap
                  >
                    {currentUser.name}
                  </Typography>
                </div>
              )}

              {/* Signout Button */}
              <Signout/>
            </Toolbar>
          </AppBar>
        </div>
      }
    </AppContext.Consumer>
  )
}

const styles = (theme: Theme) => createStyles({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1,
    display: "flex",
    alignItems: "center"
  },
  icon: {
    marginRight: theme.spacing(),
    color: "green",
    fontSize: 45
  },
  mobile: {
    display: "none"
  },
  picture: {
    height: "50px",
    borderRadius: "90%",
    marginRight: theme.spacing(2)
  }
});

export default withStyles(styles)(Header);

