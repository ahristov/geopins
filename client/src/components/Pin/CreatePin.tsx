import React, { useState, useContext, SyntheticEvent } from 'react'
import axios from 'axios'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
import TextField from "@material-ui/core/TextField"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import AddAPhotoIcon from "@material-ui/icons/AddAPhotoTwoTone"
import LandscapeIcon from "@material-ui/icons/LandscapeOutlined"
import ClearIcon from "@material-ui/icons/Clear"
import SaveIcon from "@material-ui/icons/SaveTwoTone"
import AppContext, { ACTIONS } from '../../app/AppContext'
import { CREATE_PIN_MUTATION } from '../../graphql/mutations'
import { useClient } from '../../shared/graphql'

const CreatePin: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  const graphql = useClient()
  const [title, setTitle] = useState("")
  const [image, setImage] = useState<File | null>(null)
  const [content, setContent] = useState("")
  const [submitting, setSubmitting] = useState(false)

  const { dispatch, draft } = useContext(AppContext.Context)

  const handleDeleteDraft = (event:SyntheticEvent<any>) => {
    setTitle("")
    setImage(null)
    setContent("")
    dispatch({ type: ACTIONS.DELETE_DRAFT })
  }

  const handleImageUpload = async () => {
    if (!image) { return "" }

    const data = new FormData()
    data.append("file", image)
    data.append("upload_preset", "geopins")
    data.append("cloud_name", "dxmrwpkym")
    const res = await axios.post(
      "https://api.cloudinary.com/v1_1/dxmrwpkym/image/upload",
      data
    )

    return res.data.url
  }

  const handleSubmit = async (event:SyntheticEvent<any>) => {
    try {
      event.preventDefault()
      setSubmitting(true)
      const url = await handleImageUpload()
      const variables = { title, image: url, content, ...draft }
      const { createPin } = await graphql.request(CREATE_PIN_MUTATION, variables)
      console.log("Pin created", { variables, createPin })
      dispatch({ type: ACTIONS.CREATE_PIN, payload: createPin })
      handleDeleteDraft(event)
    } catch (err) {
      setSubmitting(false)
      console.error("Error creating pin", err)
    }
  }

  return (
    <form className={classes.form}>
      <Typography
        className={classes.alignCenter}
        component="h2"
        variant="h4"
        color="secondary"
      >
        <LandscapeIcon className={classes.iconLarge}/> Pin a Location
      </Typography>
      <div>
        <TextField
          name="title"
          label="Title"
          placeholder="Insert pin title"
          onChange={e => setTitle(e.target.value)}
        />
        <input
          accept="image/*"
          id="image"
          type="file"
          className={classes.input}
          onChange={e => setImage(e.target.files ? e.target.files[0] : null)}
        />
        <label htmlFor="image">
          <Button
            style={{ color: image ? "green" : undefined }}
            component="span"
            size="small"
            className={classes.button}
          >
            <AddAPhotoIcon/>
          </Button>
        </label>
      </div>
      <div className={classes.contentField}>
        <TextField
          name="content"
          label="Content"
          multiline
          rows="6"
          variant="outlined"
          onChange={e => setContent(e.target.value)}
        />
      </div>
      <div>
        <Button
          onClick={handleDeleteDraft}
          className={classes.button}
          variant="contained"
          color="primary"
        >
          <ClearIcon className={classes.leftIcon}/>
          Discard
        </Button>
        <Button
          type="submit"
          className={classes.button}
          variant="contained"
          color="secondary"
          disabled={!title.trim() || !content.trim() || !image || submitting }
          onClick={handleSubmit}
        >
          <SaveIcon className={classes.rightIcon}/>
          Submit
        </Button>
      </div>
    </form>
  )
}

const styles = (theme: Theme) => createStyles({
  form: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    addingBottom: theme.spacing()
  },
  contentField: {
    marginLeft: theme.spacing(),
    marginRight: theme.spacing(),
    width: "95%"
  },
  input: {
    display: "none"
  },
  alignCenter: {
    display: "flex",
    alignItems: "center"
  },
  iconLarge: {
    fontSize: 40,
    marginRight: theme.spacing()
  },
  leftIcon: {
    fontSize: 20,
    marginRight: theme.spacing()
  },
  rightIcon: {
    fontSize: 20,
    marginLeft: theme.spacing()
  },
  button: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    marginRight: theme.spacing(),
    marginLeft: 0
  }
});

export default withStyles(styles)(CreatePin);
