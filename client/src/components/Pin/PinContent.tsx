import React from 'react'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
// import Explore from "@material-ui/icons/Explore"
// import Typography from "@material-ui/core/Typography"

const PinContent: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  return <div>PinContent</div>
}

const styles = (theme: Theme) => createStyles({
  root: {
    padding: "1em 0.5em",
    textAlign: "center",
    width: "100%"
  },
  icon: {
    marginLeft: theme.spacing(),
    marginRight: theme.spacing()
  },
  text: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default withStyles(styles)(PinContent);
