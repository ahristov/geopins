import React from 'react'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
import ExploreIcon from "@material-ui/icons/Explore"
import Typography from "@material-ui/core/Typography"

const NoContent: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  return (
    <div className={classes.root}>
      <ExploreIcon className={classes.icon} />
      <Typography
        noWrap
        component="h2"
        variant="h6"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        Click on the map to add a pin
      </Typography>
    </div>
  )
}

const styles = (theme: Theme) => createStyles({
  root: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center"
  },
  icon: {
    margin: theme.spacing(),
    fontSize: "80px"
  }
});

export default withStyles(styles)(NoContent);
