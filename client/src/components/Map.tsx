import React, { useState, useEffect, useContext } from 'react'
import { withStyles, Theme, createStyles } from "@material-ui/core/styles"
import ReactMapGL, { NavigationControl, Marker, PointerEvent, MapControlEvent } from 'react-map-gl'
// import Button from "@material-ui/core/Button";
// import Typography from "@material-ui/core/Typography";
// import DeleteIcon from "@material-ui/icons/DeleteTwoTone";
import PinIcon from './PinIcon'
import AppContext, { ACTIONS } from '../app/AppContext'
import Blog from './Blog'
import { useClient } from '../shared/graphql'
import { GET_PINS_QUERY } from '../graphql/queries'

// interface IMapProps extends WithStyles<typeof styles> {
// }

interface IPosition {
  latitude: number,
  longitude: number
}

const INITIAL_VIEWPORT: IPosition & {zoom: number} = {
  latitude: 20.68613,
  longitude: -156.4422094,
  zoom: 13
}

const Map: React.FC<WithStyles<typeof styles>> = ({ classes }) => {
  const graphql = useClient()
  const { draft, dispatch, currentUser, pins } = useContext(AppContext.Context)

  useEffect(() => {
    const getPins = async () => {
      const { getPins } = await graphql.request(GET_PINS_QUERY)
      dispatch({ type: ACTIONS.GET_PINS, payload: getPins })
    }
    getPins()
    // Next line explains about graphql not in dependencies array
    // eslint-disable-next-line
  },[])

  const [viewport, setViewPort] = useState(INITIAL_VIEWPORT)
  const [userPosition, setUserPosition] = useState<IPosition | null>(null)

  useEffect(() => {
    const getUserPosition = () => {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(position => {
          const { latitude, longitude } = position.coords
          console.log(`Setting current user position to ${latitude} ${longitude}`)
          setViewPort(v => { return { ...v, latitude, longitude }}) 
            // React Hook useEffect has a missing dependency: 'viewport'. 
            // Either include it or remove the dependency array. You can also do a functional update 'setViewPort(v => ...)'
            // if you only need 'viewport' in the 'setViewPort' call  react-hooks/exhaustive-deps
          setUserPosition({ latitude, longitude })
        })
      }
    }
    getUserPosition()
  }, [currentUser])


  const handleMapClick = (event: PointerEvent) => {
    const { lngLat, leftButton } = event as PointerEvent & MapControlEvent
    if (!leftButton) return
    if (!draft) {
      dispatch({ type: ACTIONS.CREATE_DRAFT })
    }
    const[longitude, latitude] = lngLat
    dispatch({ type: ACTIONS.UPDATE_DRAFT_LOCATION, payload: {longitude, latitude}})
  }

  return (
    <div className={classes.root}>
      <ReactMapGL
        width="100vw"
        height="calc(100vh - 64px)"
        mapStyle="mapbox://styles/mapbox/streets-v9"
        mapboxApiAccessToken="pk.eyJ1IjoiYWhyaXN0b3YiLCJhIjoiY2sydGE1ZjUwMHJwYTNvbnhoeDQ4Nm45cyJ9.5OeTIh1Pc0_EFpRAjRrWhQ"
        onViewportChange={newViewport => setViewPort(newViewport)}
        onClick={handleMapClick}
        {...viewport}
      >
        {/* Navigation Control */}
        <div className={classes.navigationControl}>
          <NavigationControl
            onViewportChange={newViewport => setViewPort(newViewport)}
          />
        </div>
        {/* Pin for the User's Current Position */}
        {userPosition && (
          <Marker
            {...userPosition}
            offsetLeft={-19}
            offsetTop={-37}
          >
            <PinIcon fontSize={40} color="red" onClick={(e) => console.log(e) }/>
          </Marker>
        )}
        {/* Draft Pin */}
        {draft && (
          <Marker
            {...draft}
            offsetLeft={-19}
            offsetTop={-37}
          >
            <PinIcon fontSize={40} color="hotpink" onClick={(e) => console.log(e) }/>
          </Marker>
        )}
        {/* Created Pins */}
        {pins && pins.map(pin => (
          <Marker
            key={pin._id}
            latitude={pin.latitude}
            longitude={pin.longitude}
            offsetLeft={-19}
            offsetTop={-37}
          >
            <PinIcon fontSize={40} color="darkblue" onClick={(e) => console.log(e) }/>
          </Marker>
        ))}
      </ReactMapGL>

      {/* Blog area to add pin content */}
      <Blog />
    </div>
  )
}

const styles = (theme: Theme) => createStyles({
  root: {
    display: "flex"
  },
  rootMobile: {
    display: "flex",
    flexDirection: "column-reverse"
  },
  navigationControl: {
    position: "absolute",
    top: 0,
    left: 0,
    margin: "1em"
  },
  deleteIcon: {
    color: "red"
  },
  popupImage: {
    padding: "0.4em",
    height: 200,
    width: 200,
    objectFit: "cover"
  },
  popupTab: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column"
  }
});

export default withStyles(styles)(Map);

