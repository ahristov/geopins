import React from 'react'
import Page from '../shared/Page'
import withRoot from '../shared/withRoot'
import Header from '../components/Header'
import Map from '../components/Map'

const HomePage: React.FC<{}> = () => {
  return (
    <Page name="home">
      <Header/>
      <Map/>
    </Page>
  )
}

export default withRoot(HomePage)
