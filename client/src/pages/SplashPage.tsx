import React from 'react'
import { Redirect } from 'react-router-dom'
import Page from '../shared/Page'
import Login from '../components/Auth/Login'
import AppContext from '../app/AppContext'

const SplashPage: React.FC<{}> = () => {
  // const { state } = useContext(AppContext.Context) // then use state.IsAuth

  return (
    <AppContext.Consumer>
      {({ isAuth }) =>
        isAuth ? <Redirect to="/"/>
          : (
            <Page name="splash">
              <Login/>
            </Page>
          )
      }
    </AppContext.Consumer>
  )
}

export default SplashPage
