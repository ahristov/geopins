type ClassesType<S extends (...args: any[]) => any, T extends ReturnType<S>> = {
  [P in keyof T]: string
};

interface WithStyles<T extends (...args: any[]) => any> {
  classes: ClassesType<T, ReturnType<T>>;
}
