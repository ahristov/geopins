import React from 'react'

interface IPageProps {
  name: string
}

const Page: React.FunctionComponent<IPageProps> = ({ name, children }) => {
  return (
    <div className="geopins-app">
      <div className="geopins-page">{children}</div>
    </div>
  )
}

export default Page
